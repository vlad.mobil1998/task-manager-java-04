# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO

**NAME**: Amster Vladislav

**E-MAIL**: vlad@Amster.ru

# SOFTWARE

- JDK 1.8
- MS WINDOWS 10 x64

# HARDWARE

- 2 GB RAM
- Intel Pentium Gold G6400 or AMD Athlon 200GE
- Any video adapter

# PROGRAM RUN
```bash
java -jar ./task-manager.jar
```